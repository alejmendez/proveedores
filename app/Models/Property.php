<?php
namespace App\Models;

use App\Models\ModelBase;

class Property extends ModelBase
{
    protected $fillable = [
        'name',
        'value',
    ];
}
