const mix = require('laravel-mix');
const path = require('path')
require('dotenv').config()

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
  .setPublicPath('/')
  .webpackConfig({
    resolve: {
      alias: {
        '@': path.resolve(__dirname, 'resources/js/src'),
        '@assets': path.resolve(__dirname, 'resources/assets'),
        '@sass': path.resolve(__dirname, 'resources/sass')
      }
    }
  })
  .react('resources/js/index.js', 'public/js')
  .sass('resources/sass/app.scss', 'public/css')
  .sourceMaps()

  // Change below options according to your requirement
if (mix.inProduction()) {
  mix.version()
  mix.webpackConfig({
    output: {
      publicPath: '/demo/template/demo-1/',
      chunkFilename: 'js/chunks/[name].[chunkhash].js'
    }
  })
  mix.setResourceRoot('/demo/template/demo-1/')
} else {
  mix.webpackConfig({
    output: {
      chunkFilename: 'js/chunks/[name].js'
    }
  })
    .options({
      hmrOptions: {
        host: 'proveedores.oo',  // site's host name
        port: 9001
      }
    })
    .webpackConfig({
      // add any webpack dev server config here
      devServer: {
        proxy: {
          host: '0.0.0.0',  // host machine ip
          port: 8080
        },
        watchOptions:{
          aggregateTimeout:200,
          poll:5000
        }
      }
    })
}
